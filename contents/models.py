from django.db import models
from pages.models import Page

class BaseContent(models.Model):
    @property
    def content_type(self):
        return self.__class__.__name__.lower()

    page = models.ForeignKey(Page, on_delete=models.RESTRICT, related_name='media_set')
    title = models.CharField(max_length=256)
    order = models.SmallIntegerField()
    counter = models.IntegerField(default=0)


class Video(BaseContent):
    video_url = models.CharField(max_length=256)
    sub_url = models.CharField(max_length=256)


class Audio(BaseContent):
    bitrate = models.IntegerField()


class Text(BaseContent):
    article = models.TextField(max_length=256)
