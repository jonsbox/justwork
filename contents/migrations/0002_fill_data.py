from django.db import migrations
from pages.models import Page
from contents.models import Audio, Video, Text


def fill_contents(apps, schema_editor):
    if Video.objects.exists():
        return

    pages = Page.objects.all().order_by('id')[:10]

    for p in pages:
        for n in range(1, 4):
            Video.objects.create(
                page=p,
                title=f'Видео {p.id}.{n}',
                video_url=f'video_url {p.id}.{n}',
                sub_url=f'sub_url {p.id}.{n}',
                order=n
            )
            Audio.objects.create(
                page=p,
                title=f'Аудио {p.id}.{n}',
                bitrate=10 * p.id + n,
                order=10 + n
            )
            Text.objects.create(
                page=p,
                title=f'Текст {p.id}.{n}',
                article=f'Текст статьи {p.id}.{n}',
                order=20 + n
            )


class Migration(migrations.Migration):

    dependencies = [
        ('contents', '0001_initial'),
        ('pages', '0002_fill_data'),
    ]

    operations = [
        migrations.RunPython(fill_contents),
    ]