from rest_framework import serializers, viewsets, routers, generics
from contents import models
from pages.models import Page


router = routers.DefaultRouter()
common_fields = ['id', 'title', 'order', 'counter', 'content_type']

class AudioSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Audio
        fields = common_fields + ['bitrate']


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Video
        fields = common_fields + ['video_url', 'sub_url']


class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Text
        fields = common_fields + ['article']


class BaseMediaListSerializer(serializers.ModelSerializer):
    def to_representation(self, instance):
        # TODO: автоматизировать сканирование моделей и сериализаторов
        if hasattr(instance, 'audio'):
            return AudioSerializer(instance=instance.audio).data
        elif hasattr(instance, 'video'):
            return VideoSerializer(instance=instance.video).data
        elif hasattr(instance, 'text'):
            return TextSerializer(instance=instance.text).data
        else:
            print('unknown type')
            return 'Неизвестный тип'


class PageSerializer(serializers.ModelSerializer):
    media_set = BaseMediaListSerializer(many=True)

    class Meta:
        model = Page
        fields = ['id', 'url', 'title', 'media_set']


class PageViewSet(viewsets.ModelViewSet):
    queryset = Page.objects.all()
    serializer_class = PageSerializer


router.register('view', PageViewSet)
