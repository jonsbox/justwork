from rest_framework import routers, serializers, viewsets
from rest_framework.pagination import PageNumberPagination
from .models import Page


router = routers.DefaultRouter()


class PageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = ['id', 'url', 'title']


class PagesPagination(PageNumberPagination):
    page_size = 3
    page_size_query_param = 'size'
    max_page_size = 100


class PagesViewSet(viewsets.ModelViewSet):
    queryset = Page.objects.all()
    serializer_class = PageSerializer
    pagination_class = PagesPagination


router.register(r'list', PagesViewSet)
