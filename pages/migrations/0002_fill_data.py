from django.db import migrations
from pages.models import Page


def fill_pages(apps, schema_editor):
    if not Page.objects.exists():
        Page.objects.bulk_create([
            Page(title=f'Статья {n}')
            for n in range(1, 11)
        ])


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(fill_pages),
    ]