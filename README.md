# README #

Одно из тестовых заданий на собеседовании

## Страницы ##
### Список страниц ###
Запрос

GET [http://127.0.0.1:8000/pages/list/](http://127.0.0.1:8000/pages/list/)

Ответ

```json
{
    "count": 10,
    "next": "http://127.0.0.1:8000/pages/list/?page=2",
    "previous": null,
    "results": [
        {
            "id": 1,
            "url": "http://127.0.0.1:8000/pages/list/1/",
            "title": "Статья 1"
        },
        {
            "id": 2,
            "url": "http://127.0.0.1:8000/pages/list/2/",
            "title": "Статья 2"
        },
        {
            "id": 3,
            "url": "http://127.0.0.1:8000/pages/list/3/",
            "title": "Статья 3"
        }
    ]
}
```

### Просмотр страницы ###
Запрос

GET [http://127.0.0.1:8000/pages/view/1/](http://127.0.0.1:8000/pages/view/1/)

Ответ

```json
{
    "id": 1,
    "url": "http://127.0.0.1:8000/pages/view/1/",
    "title": "Статья 1",
    "media_set": [
        {
            "id": 1,
            "title": "Видео 1.1",
            "order": 1,
            "counter": 0,
            "content_type": "video",
            "video_url": "video_url 1.1",
            "sub_url": "sub_url 1.1"
        },
        {
            "id": 2,
            "title": "Аудио 1.1",
            "order": 11,
            "counter": 0,
            "content_type": "audio",
            "bitrate": 11
        },
        {
            "id": 3,
            "title": "Текст 1.1",
            "order": 21,
            "counter": 0,
            "content_type": "text",
            "article": "Текст статьи 1.1"
        },
        {
            "id": 4,
            "title": "Видео 1.2",
            "order": 2,
            "counter": 0,
            "content_type": "video",
            "video_url": "video_url 1.2",
            "sub_url": "sub_url 1.2"
        },
        {
            "id": 5,
            "title": "Аудио 1.2",
            "order": 12,
            "counter": 0,
            "content_type": "audio",
            "bitrate": 12
        },
        {
            "id": 6,
            "title": "Текст 1.2",
            "order": 22,
            "counter": 0,
            "content_type": "text",
            "article": "Текст статьи 1.2"
        },
        {
            "id": 7,
            "title": "Видео 1.3",
            "order": 3,
            "counter": 0,
            "content_type": "video",
            "video_url": "video_url 1.3",
            "sub_url": "sub_url 1.3"
        },
        {
            "id": 8,
            "title": "Аудио 1.3",
            "order": 13,
            "counter": 0,
            "content_type": "audio",
            "bitrate": 13
        },
        {
            "id": 9,
            "title": "Текст 1.3",
            "order": 23,
            "counter": 0,
            "content_type": "text",
            "article": "Текст статьи 1.3"
        }
    ]
}
```
